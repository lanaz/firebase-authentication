package com.example.user.firebase

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import android.content.Intent
import android.widget.Toast
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mButtonLogin.setOnClickListener({
            if (validation()) {
                sendEmailVerifi()
                var email = mEditTextEmail.text.toString()
                var password = mEditTextPassword.text.toString()


                mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, {
                    if (it.isSuccessful) {
                        var user: FirebaseUser? = mAuth.getCurrentUser();
                        var intent: Intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                    }
                }).addOnFailureListener({
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                })

            }
        })
    }

    fun validation(): Boolean {
        var email = mEditTextEmail.text.toString()
        var password = mEditTextPassword.text.toString()

        var valid: Boolean = true


        if (password.isEmpty()) {
            mEditTextPassword.error = "Please enter your password"
            valid = false
        }
        if (email.isEmpty()) {
            mEditTextEmail.error = "Please enter your email"
            valid = false
        }

        return valid
    }
    fun sendEmailVerifi(){
        val user: FirebaseUser? = FirebaseAuth.getInstance().currentUser
        user?.sendEmailVerification()?.addOnCompleteListener {
            Toast.makeText(this, "Please Check Your Email", Toast.LENGTH_LONG).show()
        }
    }
}