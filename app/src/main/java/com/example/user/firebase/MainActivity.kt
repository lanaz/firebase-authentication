package com.example.user.firebase

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var mAuth: FirebaseAuth? = FirebaseAuth.getInstance();
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mButtonSignUp.setOnClickListener {

            if (validation()) {
                var email = mEditTextEmail.text.toString()
                var password = mEditTextPassword.text.toString()
                var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

                mAuth.createUserWithEmailAndPassword(mEditTextEmail.text.toString(), mEditTextPassword.text.toString()).addOnCompleteListener(this, {
                    if (it.isSuccessful) {
                        var user: FirebaseUser? = mAuth.getCurrentUser();
                        var intent: Intent = Intent(this, HomeActivity::class.java)
                        startActivity(intent)
                    }
                }).addOnFailureListener({
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                })
            }
        }
    }


    fun validation(): Boolean {
        var fullName = mEditTextFullName.text.toString()
        var email = mEditTextEmail.text.toString()
        var password = mEditTextPassword.text.toString()

        var valid: Boolean = true

        if (fullName.isEmpty()) {
            mEditTextFullName.error = "Please enter your name"
            valid = false
        }
        if (password.isEmpty()) {
            mEditTextPassword.error = "Please enter your password"
            valid = false
        }
        if (email.isEmpty()) {
            mEditTextEmail.error = "Please enter your email"
            valid = false
        }

        return valid
    }

}




